# A helper module to handle data in blog posts programmatically.
module BlogHelper
  # Given a author name in the form of a string, return the `Gitlab::Homepage::Team::Member` object for a person with the matching name.
  #
  # @param [string] author - the team member's name, So use `Tyler Williams` or `Lauren Barker` for example.
  # @return [Gitlab::Homepage::Team::Member] - this class is defined in `lib/team/member.rb` and exposed to us through `lib/homepage.rb`
  def author_data(author:)
    Gitlab::Homepage.team.members.detect { |member| member.name == author }
  end

  # Define reading time
  def reading_time(input)
    words_per_minute = 180
    words = input.split.size
    minutes = (words / words_per_minute).floor
    minutes.positive? ? "#{minutes} min read" : "less than 1 minute"
  end
end
